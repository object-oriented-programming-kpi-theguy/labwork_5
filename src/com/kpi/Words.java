package com.kpi;

/**
 * Created by Yuriy on 31.06.2016.
 */
public class Words {
    private Letter[] letters;
    private String Word;

    //Setters
    public void SetWord(String word){
        Word = word;
    }
    public void SetLetters(Letter[] let){
        letters = let;
    }

    //Constructor
    public Words(String W){
        SetWord(W);
        this.letters = new Letter[W.length()];
        for (int i = 0; i < W.length(); i++){
            letters[i] = new Letter(W.charAt(i));
        }
    }

    //Getters
    public String GetWord(){
        return Word;
    }
    public Letter[] GetLetters(){
        return letters;
    }
}
