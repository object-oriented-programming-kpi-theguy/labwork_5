package com.kpi;

import java.util.ArrayList;

/**
 * Created by Yuriy on 31.05.2016.
 */
public class Sentence {
    private String Sentence;
    private Words[] word;
    private ArrayList<Symbol> symbols;

    //Setter
    public void SetSentence(String sent){
        this.Sentence = sent;
    }
    public void SetWord(Words[] wo){
        word = wo;
    }
    public void SetSymbols(ArrayList<Symbol> punct){
        symbols = punct;
    }

    //Constructor
    public Sentence(String s, char p){
        SetSentence(s + p);
        String punct = ";,:-";
        ArrayList<Symbol> s1 = new ArrayList<Symbol>();
        for (int i = 0; i < s.length(); i++){
            for (int j = 0; j < punct.length(); j++){
                if (s.charAt(i) == punct.charAt(j)){
                    s1.add(new Symbol(s.charAt(i),i)); //Добавляет символы в Лист
                }
            }
        }
        s1.add(new Symbol(p, s.length())); //Добавляет точки, знаки вопроса и восклицания
        symbols = s1;
        String[] sent1 = s.split("[;,:-]\\s|\\s[;,:-]|\\s[;,:-]\\s|\\s");
        int counter = 0;
        for (int i = 0; i < sent1.length; i++){
            if(sent1[i].isEmpty()){
                counter++;
            }
        }
        int j = 0;
        this.word = new Words[sent1.length - counter];
        for (int i = 0; i < sent1.length; i++){
            if(!sent1[i].isEmpty()){
                word[j] = new Words(sent1[i]);
                j++;
            }
        }

    }

    //Getters
    public String GetSentence(){
        return Sentence;
    }
    public Words[] GetWord(){
        return word;
    }
    public ArrayList<Symbol> GetSymbols (){
        return symbols;
    }
}
