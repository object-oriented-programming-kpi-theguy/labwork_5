package com.kpi;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code
        Scanner scan = new Scanner(System.in);
        System.out.println("Input text");
        String str = scan.nextLine();
        Text t = new Text(str);
        StringBuilder ready = new StringBuilder(WordSort(t));
        System.out.println(ready);
    }

    public static StringBuilder WordSort(Text t){
        int k = 0;
        Container[] contain = new Container[0];
        for (int i = 0; i < t.GetSentences().length; i++){
            for (int j = 0; j < t.GetSentences()[i].GetWord().length; j++){
                String str = t.GetSentences()[i].GetWord()[j].GetWord();
                Container n = new Container(t.GetSentences()[i].GetWord()[j].GetWord(), NumberofVowel(str));
                contain[k] = n;
                k++;
            }
        }
        Comparator<Container> cont = new WordsComparator();
        Arrays.sort(contain, cont);

        String[] txt = new String[0];
        for (int i = 0; i < contain.length; i++){
            txt[i] = contain[i].Getword();
        }
        StringBuilder ssorted = new StringBuilder();
        for (String words:txt){
            ssorted.append(words).append(" ");
        }
        return ssorted;
    }

    public static int NumberofVowel(String t){
        String vowels = "аеіоєїяюуыёиaeoiyu";
        int counter = 0;
        for (int i = 0; i < t.length(); i++) {
            for (int j = 0; j < 17; j++) {
                if (t.charAt(i) == vowels.charAt(j)) {
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }
}
