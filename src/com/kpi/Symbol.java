package com.kpi;

/**
 * Created by Yuriy on 30.05.2016.
 */
public class Symbol {
    private char Symbol;
    private int Position;


    //Setters
    public void SetSymbol(char sym){
        Symbol = sym;
    }
    public void SetPosition(int pos){
        Position = pos;
    }

    //Constructor
    public Symbol(char a, int p){
        SetSymbol(a);
        SetPosition(p);
    }
}
