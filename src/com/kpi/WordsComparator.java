package com.kpi;

import java.util.Comparator;

/**
 * Created by Yuriy on 31.05.2016.
 */
public class WordsComparator implements Comparator<Container> {
    public int compare(Container a, Container b){
        if (a.GetVowel() > b.GetVowel())
            return 1;
        else if (a.GetVowel() < b.GetVowel())
            return -1;
        return 0;
    }
}
