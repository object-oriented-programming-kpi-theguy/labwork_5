package com.kpi;

import java.util.Scanner;

/**
 * Created by Yuriy on 30.05.2016.
 */
public class Text {
    private String MyText;
    private Sentence[] sentences;

    //Setters
    public void SetText(String txt){
        MyText = txt;
    }
    public void SetSentence(Sentence[] sent){
        sentences = sent;
    }

    //Constructor
    public Text(String t){
        String txt = t.replaceAll("\\s+", " "); //Минус лишние пробелы
        SetText(txt);
        String[] s1 = txt.split("\\.|!|\\?");
        sentences = new Sentence[s1.length];
        int k = 0;
        for (int i = 0; i < s1.length; i++){
            k = k + s1[i].length();
            sentences[i] = new Sentence(s1[i], txt.charAt(k + i));
        }
    }

    //Getters
    public String GetText(){
        return MyText;
    }
    public Sentence[] GetSentences(){
        return sentences;
    }
}
