package com.kpi;

/**
 * Created by Yuriy on 30.05.2016.
 */
public class Letter {
    private char Myletter;

    //Setter
    public void SetLetter(char s){
        Myletter = s;
    }

    //Constructor
    public Letter(char let){
        Myletter = let;
    }

    //Getter
    public char GetLetter(){
        return Myletter;
    }
}
