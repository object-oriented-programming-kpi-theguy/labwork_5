package com.kpi;

/**
 * Created by Yuriy on 01.06.2016.
 */
public class Container {
    private String word;
    private int vowels;

    //Setters
    public void SetWord(String world){
        word = world;
    }
    public void SetVowels(int vow){
        vowels = vow;
    }

    //Constructor
    public Container(String w, int v){
        SetWord(w);
        SetVowels(v);
    }

    //Getters
    public String Getword(){
        return word;
    }
    public int GetVowel(){
        return vowels;
    }
}
